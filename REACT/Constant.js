/*eslint-disable max-len */
/*eslint-disable no-useless-escape */
import NumberUtils from './utils/NumberUtils';

const Action = {
    CREATE_LOAN: 'CREATE_LOAN',
    CREATE_INVEST: 'CREATE_INVEST',
    SETTLE_LOAN: 'SETTLE_LOAN'
};

const Api = {
    DECLARE_ADDRESS: 'address',
    DECLARE_BIRTH: 'birth',
    DECLARE_CITY: 'city',
    DECLARE_EMAIL: 'email',
    DECLARE_INCOME: 'income',
    DECLARE_IMG: 'img',
    DECLARE_IMG_SELFIE: 'selfieImg.jpg',
    DECLARE_IMG_SSN_BACK: 'ssnBackImg.jpg',
    DECLARE_IMG_SSN_FRONT: 'ssnFrontImg.jpg',
    DECLARE_IMG_TYPE: 'image/jpg',
    DECLARE_JOB: 'job',
    DECLARE_NAME: 'name',
    DELCARE_SEX: 'sex',
    DECLARE_SSN: 'ssn',
    TOKEN: 'x-auth',
    TYPE_BORROWER: 'borrower',
    TYPE_LENDER: 'lender'
};

const Bcrypt = {
    SALT: '$2a$10$YiTI4lGejge8/fOVZ/pcau'
};

const Currency = {
    UNIT_VN: '$'
};

const DateTime = {
    FORMAT_DISPLAY: 'DD.MM.YYYY',
    FORMAT_FIREBASE: 'DD-MM-YYYY',
    FORMAT_ISO: 'YYYY-MM-DDTHH:mm:ss.sssZ',
    FORMAT_SEND: 'MM.DD.YYYY'
};

const Default = {
    CODE: '1',
    TRANSACTION_WEB_LINK: 'https://aqueous-hollows-61580.herokuapp.com/#/tx/'
};

const Maximum = {
    CARD_NUM: 19,
    LOAN: 50000000,
    NUM_MONTH: 18,
    SCORE: 890
};

const Minimum = {
    CARD_NUM: 16,
    CODE: 6,
    INCOME: 100000,
    LOAN: 1000000,
    LOAN_NODE: 500000,
    MATURITY_DATE: 10,
    NUM_MONTH: 1,
    // PASSWORD: 6,
    PHONE: 8,
    SCORE: 580
};

const ErrorMsg = {
    CARD_NOT_ENOUGH: `Card number must be from ${Minimum.CARD_NUM} to ${Maximum.CARD_NUM} in card `,
    CARD_INVALID: 'Invalid card number',
    CODE_INVALID: 'Incorrect code',
    CODE_NOT_ENOUGH: `Verification code must be correct ${Minimum.CODE} characters`,
    COMMON: 'An error occurred\nPlease try again',
    EMAIL_INVALID: 'Invalid email',
    FIELD_MISSING: 'Please fill in all information',
    INCOME_INVALID: `Income must be greater than ${NumberUtils.addMoneySeparator(Minimum.INCOME.toString())}${Currency.UNIT_VN}/month`,
    LOAN_INVALID: `The loan amount must be a multiple of ${NumberUtils.addMoneySeparator(Minimum.LOAN_NODE.toString())}${Currency.UNIT_VN} và trong khoảng ${NumberUtils.addMoneySeparator(Minimum.LOAN.toString())}${Currency.UNIT_VN} - ${NumberUtils.addMoneySeparator(Maximum.LOAN.toString())}${Currency.UNIT_VN}`,
    NAME_INVALID: 'Name is not valid',
    NETWORK_INVALID: 'No internet connection\nPlease try again',
    NUM_MONTH_INVALID: `the loan month must be between\n${NumberUtils.addMoneySeparator(Minimum.NUM_MONTH.toString())} months and ${NumberUtils.addMoneySeparator(Maximum.NUM_MONTH.toString())} months`,
    PASSWORD_CONFIRM_FALSE: 'Confirm Password No Match',
    // PASSWORD_NOT_ENOUGH: `Password must contain at least ${Minimum.PASSWORD} characters`,
    PHONE_INVALID: 'Invalid phone number',
    PHONE_NOT_ENOUGH: `Phone number must have at least ${Minimum.PHONE} number`,
    SSN_INVALID: 'Invalid ID number'
};

const RegularExp = {
    CARD: /^[0-9-]*$/,
    EMAIL: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    NUMBER: /^[0-9]*$/,
    NUMBER_SEPARATOR: /^[0-9,]*$/,
    PHONE: /^[0-9]*$/,
    SSN: /^(\d{9}|\d{12})$/
};

const Sort = {
    TYPE_RATE: 'rate',
    TYPE_CAPITAL: 'capital',
    TYPE_INTEREST: 'interest',
    TYPE_INVESTING_DAY_LEFT: 'investingDayLeft'
};

export {
    Action,
    Api,
    Bcrypt,
    Currency,
    DateTime,
    Default,
    Maximum,
    Minimum,
    ErrorMsg,
    RegularExp,
    Sort
};
